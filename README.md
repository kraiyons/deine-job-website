A simple mobile only view of a landing page written in Nuxt and Tailwind

#### Url

https://workforce.kraiyons.dev/

#### Todo:

- [x] Basic layout
- [x] Hero section
- [x] CTA component
- [x] Header component
- [x] Steps component
- [x] Fix overlapping issue of header and main section
- [x] CTA hide until scrolled down
- [ ] Color transition change on category click

Copyright 2021 Krai Alapide (<krai.alapide@gmail.com>)
